<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class controllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @group users
     * @test
     */
    public function load_the_user_profile_view(){

        $user = factory(User::class)->create();

        $response = $this->get('user/' . $user->id);
        $response->assertStatus(200);
        $response->assertViewIs('user.profile');
        $response->assertSee($user->name);
    }

    /**
     * @group users
     * @test
     */
    public function load_user_profile_single_controller(){

        $user = factory(User::class)->create();

        $response = $this->get('user-sc/' . $user->id);
        $response->assertStatus(200);
        $response->assertViewIs('user.profile-sc');
        $response->assertSee($user->name);
    }
}


<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
//Authentication Routes
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//Controllers
Route::get('user/{id}', 'UserController@show');

//Controllers & Namespaces
#Route::get('foo', 'Photos\AdminController@method');

//Single Action Controllers
Route::get('user-sc/{id}', 'ShowProfile');

//Controller Middleware
Route::get('profile', 'UserController@profile')->middleware('auth');

//Resource Controllers
Route::resource('photos', 'PhotoController');

//Partial Resource Routes
Route::resource('photos', 'PhotoController')->only([
    'index', 'show'
]);

Route::resource('photos', 'PhotoController')->except([
    'create', 'store', 'update', 'destroy'
]);

//API Resource Routes
Route::apiResource('photos', 'PhotoController');

Route::apiResources([
    'photos' => 'PhotoController',
    //'posts' => 'PostController'
]);

//Naming Resource Routes
Route::resource('photos', 'PhotoController')->names([
    'create' => 'photos.build'
]);

//Naming Resource Route Parameters
Route::resource('users', 'AdminUserController')->parameters([
    'users' => 'admin_user'
    #/users/{admin_user}
]);

//Localizing Resource URIs
#/fotos/crear
#/fotos/{foto}/editar

//Supplementing Resource Controllers
Route::get('photos/popular', 'PhotoController@method');

Route::resource('photos', 'PhotoController');

//Method Injection with additional parameters
Route::put('user/{id}', 'UserController@update');

//Route Caching
#php artisan route:cache
#php artisan route:clear

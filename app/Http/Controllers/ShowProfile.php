<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class ShowProfile extends Controller
{
    /**
     * Show the profile for the given user.
     * #Artisan command => php artisan make:controller ShowProfile --invokable
     * @param  int  $id
     * @return View
     */
    public function __invoke($id)
    {
        return view('user.profile-sc', ['user' => User::findOrFail($id)]);
    }
}

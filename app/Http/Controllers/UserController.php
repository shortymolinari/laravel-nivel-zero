<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        // $this->middleware('auth');

        // $this->middleware('log')->only('index');

        // $this->middleware('subscribed')->except('store');

    }

    //it is not recommended (SRP)
    /*$this->middleware(function ($request, $next) {
        // ...
        return $next($request);
    });*/

    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return View
     */
    public function show($id){
        return view('user.profile', ['user' => User::findOrFail($id)]);
    }



}
